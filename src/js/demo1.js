import '../css/demo1.scss';


import mcRatio from '../../lib/mc-ratio';

mcRatio({
  size : [750, 1334],      // 设计稿的尺寸
  full : true,             // 是否开启全屏模式？如果开启开屏模式则对宽度和高度双向自适应
  dpr  : true,             // 是否开启DPR,开启后尺寸将设置为物理分辨率
  fixed: 0                 // 字号缩放精度为小数点后2位
});
