import '../css/demo2.scss';
import mcRatio from '../../lib/mc-ratio';
// 代码着色插件
import McTinting from '@fekit/mc-tinting';

new McTinting({
  theme: 'vscode',
  number: 1
});

mcRatio({
  el: document.getElementById('area'),
  size: [
    750,
    1334
  ],
});
