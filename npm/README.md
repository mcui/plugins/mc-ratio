#MC-RATIO

一个移动端大小屏自适应方案支持插件,可以横向自适应和横竖双向自适应，包括根字号全局适配和标签区域内局部自适应。并且可以开启DPR让网页UI达到原生APP的细腻程度



## 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)


## 演示
[https://fekit.asnowsoft.com/plugins/mc-ratio/](https://fekit.asnowsoft.com/plugins/mc-ratio/)


## 开始

下载项目: 


```git
git clone https://gitlab.com/fekits/mc-ratio.git
```

```
npm i mc-ratio

```

## 参数
```
    {
        el   : 'id',               {DomObject}   DOM对象，如果不填则默认为HTML根标签
        size : [750, 1334],        {Number}      设计稿的尺寸
        full : true | false,       {Boolean}     是否开启全屏模式？如果开启开屏模式则对宽度和高度双向自适应 *
        dpr:   true | false        {Boolean}     开启DPR功能,开启DPR后网页尺寸将设置为设备物理分辨率，网页1px可以达到原生APP细腻程度
        fixed: 2                   {Number}      设置字号精度为小数点后2，一般建议为2位
        then : function () {}      {Function}    自适应字号改变时的回调
        max  : 750                 {Number}      最大尺寸，超过这个尺寸不再放大
        min  : 320                 {Number}      最小尺寸，超过这个尺寸不再缩小
    }
```

## 示例
基础自适应：
```javascript
ratio({
  size:  [750, 1334],                  // 设计稿的尺寸
});
```

双向自适应：
```javascript
ratio({
  size:  [750, 1334],                  // 设计稿的尺寸
  full:  true,                         // 开启全屏模式，将对宽度和高度双向自适应缩放，这种模式一般用于无滚动条的全屏页面。
  fixed: 2                             // 设置字号精度为小数点后2位
});
```

DPR高清方案：
```javascript
ratio({
  size:  [750, 1334],                  // 设计稿的尺寸
  full:  true,                         // 开启全屏模式，将对宽度和高度双向自适应缩放，这种模式一般用于无滚动条的全屏页面。
  dpr:   true,                         // 开启DPR功能,物理像素细腻度
});
```

限制最大尺寸：
```javascript
ratio({
  size:  [750, 1334],                  // 设计稿的尺寸
  full:  true,                         // 开启全屏模式，将对宽度和高度双向自适应缩放，这种模式一般用于无滚动条的全屏页面。
  max:   750,                          // 超过750的尺寸不再放大
});
```

局部自适应
```javascript
// 以下示例仅对指定ID为area的元素做自适应，需要注意的是，如果你仅对网页中的部分区域做自适应，请勿开启dpr功能，该功能将影响全局网页
ratio({
  el:    document.getElementId('area'),    // 为ID为area的标签设置自适应字号，当字号不是设置在HTML根标签时，只能在区域内用em方案自适应
  size:  [750, 1334],                      // 设计稿的尺寸
  fixed: 2
});
```

## 版本
```
v1.0.5 [Latest version]
1、整理文档与DEMO
```

```
v1.0.3 [Latest version]
1、新增了max和min设置最大和最小缩放尺寸
```

```
v1.0.2
1、新增了dpr功能，开启了DPR功能后，可以自动设置设备尺寸为物理分辨率使网页达到原生APP精细效果。就是解决经典的1px太粗问题
2、新增了fixed参数可以设置字号缩放时的小数字精度。
```

```
v1.0.0
1、实现了横向自适应和横竖双向自适应，并且竖向自适应可自由设定阀值。
2、可以设置根字号全局适配和标签区域内局部自适应。
```